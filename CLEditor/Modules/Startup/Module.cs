﻿using System.ComponentModel.Composition;
using Gemini.Framework;
using Gemini.Framework.Menus;

namespace CLEditor.Modules.Startup
{
    [Export(typeof(IModule))]
    public class Module : ModuleBase
    {
        [Export] public static MenuDefinition Menu =
            new MenuDefinition(Gemini.Modules.MainMenu.MenuDefinitions.MainMenuBar, 5, "测试");

        [Export] public static MenuItemGroupDefinition MenuGroup = new MenuItemGroupDefinition(Menu, 0);

        public override void Initialize()
        {
            MainWindow.Title = "CLEditor";
        }
    }
}