﻿
using System.ComponentModel.Composition;
using CLEditor.Modules.Commands;
using Gemini.Framework.Menus;

namespace CLEditor.Modules
{
    public static class MenuDefinitions
    {
        [Export] public static MenuItemDefinition ViewSceneViewerMenuItem =
            new CommandMenuItemDefinition<ViewSceneViewerCommandDefinition>(Startup.Module.MenuGroup, 1);
    }
}