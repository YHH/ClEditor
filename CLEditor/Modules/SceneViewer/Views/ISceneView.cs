﻿namespace CLEditor.Modules.SceneViewer.Views
{
    public interface ISceneView
    {
        void Invalidate();
    }
}