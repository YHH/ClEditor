﻿using System;
using System.Windows.Controls;
using System.Windows.Input;
using Caliburn.Micro;
using Gemini.Modules.MonoGame.Controls;
using Gemini.Modules.Output;
using Microsoft.Xna.Framework;

namespace CLEditor.Modules.SceneViewer.Views
{
    /// <summary>
    /// SceneView.xaml 的交互逻辑
    /// </summary>
    public partial class SceneView : UserControl, ISceneView, IDisposable
    {
        private readonly IOutput _output;

        public SceneView()
        {
            InitializeComponent();

            _output = IoC.Get<IOutput>();
        }

        public void Invalidate()
        {
            GraphicsControl.Invalidate();
        }

        /// <summary>执行与释放或重置非托管资源关联的应用程序定义的任务。</summary>
        public void Dispose()
        {
            GraphicsControl.Dispose();
        }

        private void GraphicsControl_OnLoadContent(object sender, GraphicsDeviceEventArgs e)
        {
        }

        private void GraphicsControl_OnDraw(object sender, DrawEventArgs e)
        {
            e.GraphicsDevice.Clear(Color.CornflowerBlue);
        }

        private void GraphicsControl_OnMouseMove(object sender, MouseEventArgs e)
        {
        }

        private void GraphicsControl_OnMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
        }

        private void GraphicsControl_OnMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
        }

        private void GraphicsControl_OnKeyDown(object sender, KeyEventArgs e)
        {
        }

        private void GraphicsControl_OnKeyUp(object sender, KeyEventArgs e)
        {
            _output.AppendLine("Key up" + e.Key);
        }
    }
}
