﻿using System;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Primitives;
using CLEditor.Modules.SceneViewer.Views;
using Gemini.Framework;
using Microsoft.Xna.Framework;

namespace CLEditor.Modules.SceneViewer.ViewModels
{
    [Export(typeof(SceneViewModel))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class SceneViewModel : Document
    {
        private ISceneView _sceneView;

        public override bool ShouldReopenOnStart
        {
            get { return true; }
        }

        private Vector3 _position;

        public Vector3 Position
        {
            get { return _position; }
            set
            {
                _position = value;
                NotifyOfPropertyChange(() => Position);

                if (_sceneView != null)
                {
                    _sceneView.Invalidate();
                }
            }
        }

        public SceneViewModel()
        {
            DisplayName = "场景视图";
        }

        /// <summary>Called when an attached view's Loaded event fires.</summary>
        /// <param name="view"></param>
        protected override void OnViewLoaded(object view)
        {
            _sceneView = view as ISceneView;
            base.OnViewLoaded(view);
        }

        /// <summary>Called when deactivating.</summary>
        /// <param name="close">Inidicates whether this instance will be closed.</param>
        protected override void OnDeactivate(bool close)
        {
            if (close)
            {
                var view = GetView() as IDisposable;
                if (view != null)
                {
                    view.Dispose();
                }
            }

            base.OnDeactivate(close);
        }
    }
}