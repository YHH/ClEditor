﻿using Gemini.Framework.Commands;

namespace CLEditor.Modules.Commands
{
    [CommandDefinition]
    public class ViewSceneViewerCommandDefinition : CommandDefinition
    {
        public const string CommandName = "CLEditor.SceneViewer";

        public override string Name
        {
            get { return CommandName; }
        }
        public override string Text
        {
            get { return "视图"; }
        }
        public override string ToolTip
        {
            get { return "视图"; }
        }
    }
}